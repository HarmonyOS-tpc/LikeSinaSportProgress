/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dreamer.likesinasportprogress;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 按下与非按下状态时显示不同图片的自定义Image
 */
public class SelectorImage extends Image implements Component.TouchEventListener {

    private final String TOUCH_ON = "touch_on"; // 自定义属性，按下时的资源id
    private final String TOUCH_OFF = "touch_off"; // 自定义属性，没按下时的资源id

    private int touchOnRes = 0;
    private int touchOffRes = 0;

    private ClickedListener clickedListener;
    private TouchEventListener touchEventListener;

    public SelectorImage(Context context) {
        this(context, null);
    }

    public SelectorImage(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SelectorImage(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        if (attrSet != null) {
            if (attrSet.getAttr(TOUCH_ON).isPresent()) {
                touchOnRes = attrSet.getAttr(TOUCH_ON).get().getIntegerValue();
            }
            if (attrSet.getAttr(TOUCH_OFF).isPresent()) {
                touchOffRes = attrSet.getAttr(TOUCH_OFF).get().getIntegerValue();
            }
        }
        super.setTouchEventListener(this);
    }

    /**
     * 设置按下与非按下状态时显示的图片资源id
     *
     * @param touchOnRes  按下状态时显示的图片资源id
     * @param touchOffRes 非按下状态时显示的图片资源id
     */
    public void setSelectorImageRes(int touchOnRes, int touchOffRes) {
        this.touchOnRes = touchOnRes;
        this.touchOffRes = touchOffRes;
        setPixelMap(touchOffRes);
    }

    /**
     * 设置控件点击事件监听器
     *
     * @param clickedListener 点击事件监听器
     */
    @Override
    public void setClickedListener(ClickedListener clickedListener) {
        this.clickedListener = clickedListener;
    }

    /**
     * 设置控件触摸事件监听器
     *
     * @param touchEventListener 触摸事件监听器
     */
    @Override
    public void setTouchEventListener(TouchEventListener touchEventListener) {
        this.touchEventListener = touchEventListener;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                setPixelMap(touchOnRes);
                invalidate();
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                setPixelMap(touchOffRes);
                invalidate();
                clickedListener.onClick(component);
                break;
            default:
                break;
        }
        if (touchEventListener != null) {
            return touchEventListener.onTouchEvent(component, touchEvent);
        }
        return true;
    }

}
