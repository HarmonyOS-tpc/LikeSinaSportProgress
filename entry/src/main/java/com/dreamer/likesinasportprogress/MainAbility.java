package com.dreamer.likesinasportprogress;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * 带点赞功能的左右进度条演示页面
 */
public class MainAbility extends Ability {

    private SinaSportLayout mSinaSportLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        mSinaSportLayout = (SinaSportLayout) findComponentById(ResourceTable.Id_layout);
        mSinaSportLayout.setOnSinaSportListener(new SinaSportLayout.OnSinaSportListener() {
            @Override
            public void onLeftClick(Text tvLeft) {
                mSinaSportLayout.incrementLeftProgressValue(20);
                tvLeft.setText(mSinaSportLayout.getLeftProgressValue() + "");
            }

            @Override
            public void onRightClick(Text tvRight) {
                mSinaSportLayout.incrementRightProgressValue(20);
                tvRight.setText(mSinaSportLayout.getRightProgressValue() + "");
            }
        });
        findComponentById(ResourceTable.Id_btn).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                startAbility(MainAbility.this, RatioProgressActivity.class);
            }
        });
    }

    private void startAbility(Ability aAbility, Class bAbility) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(aAbility.getBundleName())
                .withAbilityName(bAbility.getName())
                .build();
        secondIntent.setOperation(operation);
        aAbility.startAbility(secondIntent);
    }

}
