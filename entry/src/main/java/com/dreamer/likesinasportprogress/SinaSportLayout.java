package com.dreamer.likesinasportprogress;

import com.dreamer.ratioprogresslibrary.RatioProgressBar;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Created by ysx on 2016/7/2.
 */
public class SinaSportLayout extends DirectionalLayout {

    private RatioProgressBar mProgress;
    private Text mTvLeft;
    private Text mTvRight;

    private OnSinaSportListener mListener;

    public SinaSportLayout(Context context) {
        this(context, null);
    }

    public SinaSportLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SinaSportLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_progress_layout, this, true);

        SelectorImage imgLeft = (SelectorImage) findComponentById(ResourceTable.Id_imgLeft);
        imgLeft.setSelectorImageRes(ResourceTable.Media_ic_left_zan_p, ResourceTable.Media_ic_left_zan_n);

        mProgress = (RatioProgressBar) findComponentById(ResourceTable.Id_progress);
        mTvLeft = (Text) findComponentById(ResourceTable.Id_tv_left_text);
        mTvRight = (Text) findComponentById(ResourceTable.Id_tv_right_text);

        SelectorImage imgRight = (SelectorImage) findComponentById(ResourceTable.Id_imgRight);
        imgRight.setSelectorImageRes(ResourceTable.Media_ic_right_zan_p, ResourceTable.Media_ic_right_zan_n);

        imgLeft.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 开启按钮缩放动画
                btnAnim(component);
                if (mListener != null) {
                    mListener.onLeftClick(mTvLeft);
                }
            }
        });
        imgRight.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                btnAnim(component);
                if (mListener != null) {
                    mListener.onRightClick(mTvRight);
                }
            }
        });

        mTvLeft.setText(mProgress.getLeftProgressValue() + "");
        mTvRight.setText(mProgress.getRightProgressValue() + "");

    }

    /**
     * 设置点赞按钮的点击事件监听器
     *
     * @param listener 点赞按钮的点击事件监听器
     */
    public void setOnSinaSportListener(OnSinaSportListener listener) {
        mListener = listener;
    }

    private void btnAnim(Component view) {
        AnimatorProperty anim1 = view.createAnimatorProperty().scaleXFrom(1.0f).scaleX(1.2f).scaleYFrom(1.0f).scaleY(1.2f).setDuration(250);
        AnimatorProperty anim2 = view.createAnimatorProperty().scaleXFrom(1.2f).scaleX(1.0f).scaleYFrom(1.2f).scaleY(1.0f).setDuration(250);
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroup.runSerially(anim1, anim2);
        animatorGroup.start();
    }

    /**
     * 左边进度条增加进度
     *
     * @param value 增加的进度值
     */
    public void incrementLeftProgressValue(int value) {
        if (mProgress != null) {
            mProgress.setLeftProgressValue(value);
        }
    }

    /**
     * 右边进度条增加进度
     *
     * @param value 增加的进度值
     */
    public void incrementRightProgressValue(int value) {
        if (mProgress != null) {
            mProgress.setRightProgressValue(value);
        }
    }

    /**
     * 获取左边进度条的进度值
     *
     * @return 左边进度条的进度值
     */
    public int getLeftProgressValue() {
        if (mProgress != null) {
            return mProgress.getLeftProgressValue();
        }
        return 0;
    }

    /**
     * 获取右边进度条的进度值
     *
     * @return 右边进度条的进度值
     */
    public int getRightProgressValue() {
        if (mProgress != null) {
            return mProgress.getRightProgressValue();
        }
        return 0;
    }

    /**
     * 点赞按钮的点击事件监听器
     */
    public interface OnSinaSportListener {
        void onLeftClick(Text tvLeft);

        void onRightClick(Text tvRight);
    }

}
