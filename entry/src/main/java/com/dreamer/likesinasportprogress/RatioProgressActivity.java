package com.dreamer.likesinasportprogress;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * 单纯的左右进度条演示页面
 */
public class RatioProgressActivity extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_ratio_progress);
    }

}
