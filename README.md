﻿## Preview
<img src="https://gitee.com/HarmonyOS-tpc/LikeSinaSportProgress/raw/master/screenshot/LikeSinaSportProgress.gif" width="40%"/>

## Gradle
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:LikeSinaSportProgress:1.0.1'
```

## Usage
```java
<com.dreamer.ratioprogresslibrary.RatioProgressBar
            ohos:height="7vp"
            ohos:width="match_parent"
            ohos:left_progress_bg="#DD2F1C"
            ohos:left_progress_value="1580"
            ohos:left_right_progress_spacing="1"
            ohos:progress_anim_duration="4000"
            ohos:progress_height="7vp"
            ohos:right_progress_bg="#1D69E1"
            ohos:right_progress_value="800"/>
```

## Attributes
| name                        |  format   | description               | default_value |
| :--------------------------:| :------:  | :-----------:             | :-----------: |
| left_progress_bg            | color     | 左进度条的背景颜色          | #DD2F1C       |
| left_progress_value         | integer   | 左进度条的大小             | 0 |
| right_progress_bg           | color     | 右进度条的背景颜色         | #1D69E1|
| right_progress_value        | integer   | 右进度条的大小             | 0 |
| left_right_progress_spacing | integer   | 左右进度条之间的间隔距离(间隔 = n * progress_height)    | n = 1 |
| progress_anim_duration      | integer   | 进度条的动画时长（单位毫秒） | 3000 |
| progress_height             | dimension | 进度条的高度                | 15 |

## entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## License
```
   Copyright 2016 Dreamer

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
